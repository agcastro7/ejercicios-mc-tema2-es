<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tema 2 - Ejercicio 17</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Ejercicio 17 - Formularios</h1>
    <div class="caja enunciado">
      Escribe un formulario que envíe mediante GET al fichero ejercicio17-b.php los siguientes datos:
        <ul>
          <li>El nombre de usuario</li>
          <li>La contraseña</li>
          <li>La contraseña repetida</li>
          <li>Si acepta los términos y condiciones</li>
          <li>Un desplegable con el módulo que estudia, que ofrezca las opciones: Diseño de Aplicaciones Web, Diseño de Aplicaciones Multiplataforma, Administración de Sistemas Informáticos.</li>
        </ul>
    </div>
    <div class="caja solucion">
        <h3>Solución</h3>
        
    </div>
  </body>
</html>

<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tema 2 - Ejercicio 15</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Ejercicio 15 - Arrays</h1>
    <div class="caja enunciado">
      Escribe una función que calcule la media de los elementos de un array. Si el array está vacío, devolverá un cero. Si el array tiene elementos que no son numéricos, los ignorará. <br>
      Prueba la función con los siguientes arrays:
      <div class="code">
        <ul>
          <li>[1, 2, 3, 4] Solución: 2.5</li>
          <li>["Hola", false, 3, 6] Solución: 4.5</li>
          <li>null Solución: 0</li>
          <li>["Hola", "Adios"] Solución: 0</li>
        </ul>
      </div>
      
    </div>
    <div class="caja solucion">
        <h3>Solución</h3>
        <?php
        
        ?>
    </div>
  </body>
</html>

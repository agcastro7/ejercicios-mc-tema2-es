<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tema 2 - Ejercicio 19</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Ejercicio 19 - Formulario con autocomprobación de errores</h1>
    <div class="caja enunciado">
      Escribe un formulario que envíe los siguientes datos:
      <ul>
        <li>Un correo electrónico</li>
        <li>Una contraseña</li>
        <li>Una confirmación de contraseña</li>
      </ul>
      El destinatario de los datos debe ser él mismo. Los datos pueden presentar los siguientes errores:
      <ul>
        <li>Que el correo electrónico no tenga una @.</li>
        <li>Que la contraseña sea menor de 6 caracteres.</li>
        <li>Que las contraseñas no coincidan.</li>
      </ul>
      La primera vez que se abre la página, mostrará el formulario en blanco. <br>
      Una vez pulsado el botón de enviar, si la información es correcta, no mostrará el formulario, sino un mensaje que indique que el registro ha tenido éxito. En caso contrario, mostrará de nuevo el formulario, relleno con los datos que se han enviado, y con un mensaje de error explicativo junto a los campos incorrectos.
    </div>
    <div class="caja solucion">
        <h3>Solución</h3>
        <?php
        ?>
    </div>
  </body>
</html>

<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tema 2 - Ejercicio 18</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Ejercicio 18 - Comprobación de errores</h1>
    <div class="caja enunciado">
      Escribe un script que reciba los datos del formulario anterior, y se asegure de que:
      <ul>
        <li>El número de teléfono no está vacío</li>
        <li>El número de teléfono sólo tiene caracteres numéricos</li>
        <li>Al menos una de las casillas está marcada</li>
        <li>El texto de la dirección no está vacío</li>
        <li>El texto de la dirección no supera los 300 caracteres</li>
      </ul>
      Si encuentra algún error, debe notificarlo por pantalla. Si no, debe mostrar un mensaje indicando que todo es correcto.
    </div>
    <div class="caja solucion">
        <h3>Solución</h3>
        <?php
        
        ?>
    </div>
  </body>
</html>

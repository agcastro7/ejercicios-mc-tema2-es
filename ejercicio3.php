<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tema 2 - Ejercicio 3</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Ejercicio 3 - Cadenas de varias líneas</h1>
    <div class="caja enunciado">
      Almacena en una variable una cadena de caracteres que tenga varias líneas <strong>sin usar la etiqueta br</strong>, como por ejemplo la siguiente: <br><br>
      <div class="center">
        <em>
        Yo soy la mulata, tengo la boca de plata <br>
        Yo que estaba enamorata, te cantaba serenatas <br>
        Acordate que soy Natalia, reina de la vigilia <br>
        Deja que te combata ya, ¡Ah, no! Te hace falta corashe <br>
        Te hace falta corashe <br><br>
        </em>
      </div>
      Muéstrala con un echo o un print.
    </div>
    <div class="caja solucion">
        <h3>Solución</h3>
        <?php
        
        ?>
    </div>
  </body>
</html>

<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tema 2 - Ejercicio 4</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Ejercicio 4 - Conversión de tipos</h1>
    <div class="caja enunciado">
      En el código php de la solución tienes una serie de variables con diferentes valores, ¿qué tipo crees que tiene cada una?. Compruébalo mostrando los tipos por pantalla (completa los print que aparecen). <br>
      A continuación, cambia el tipo de la variable a para que sea float, b para que sea una cadena de caracteres c para que sea entera, d para que sea booleana y e para que sea un entero. Comprueba cómo cambian los mensajes.
    </div>
    <div class="caja solucion">
        <h3>Solución</h3>
        <?php
          $a = 12;
          $b = 12.2;
          $c = "12";
          $d = 1;
          $e = false;

          // Completa los print a continuación
          print "El tipo de la variable a ($a) es: ";
          print "<br>";
          print "El tipo de la variable b ($b) es: ";
          print "<br>";
          print "El tipo de la variable c ($c) es: ";
          print "<br>";
          print "El tipo de la variable d ($d) es: ";
          print "<br>";
          print "El tipo de la variable e ($e) es: ";
          print "<br>";
        ?>
    </div>
  </body>
</html>

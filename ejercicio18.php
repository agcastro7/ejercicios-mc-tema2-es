<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tema 2 - Ejercicio 18</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Ejercicio 18 - Formulario con array</h1>
    <div class="caja enunciado">
      Escribe un formulario que envíe mediante POST los siguientes datos al fichero ejercicio18-b.php:
      <ul>
        <li>Un número de teléfono</li>
        <li>Varias casillas que indican diferentes provincias de Andalucía (deben enviar los datos en un array)</li>
        <li>Un cuadro de texto en el que escribir una dirección de varias líneas.</li>
      </ul>
    </div>
    <div class="caja solucion">
        <h3>Solución</h3>
        
    </div>
  </body>
</html>

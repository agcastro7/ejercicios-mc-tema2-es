<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tema 2 - Ejercicio 12</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Ejercicio 12 - Bucles III</h1>
    <div class="caja enunciado">
      Escribe un script que genere un número aleatorio entre 3 y 5, y a continuación muestre una pirámide con ese número de niveles. La pirámide estará hecha de asteriscos. Por ejemplo, si el resultado es 4, una pirámide de 4 niveles sería: <br><br>
      * <br>
      ** <br>
      *** <br>
      **** <br>
    </div>
    <div class="caja solucion">
        <h3>Solución</h3>
        <?php
        
        ?>
    </div>
  </body>
</html>

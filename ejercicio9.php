<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tema 2 - Ejercicio 9</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Ejercicio 9 - Condicionales II</h1>
    <div class="caja enunciado">
      Las peticiones HTTP que se realizan a los servidores suelen recibir una respuesta acompañada de un código informativo. Los código más habituales son:
        <ul>
          <li><strong>200:</strong> OK. Es la respuesta estándar para peticiones correctas.</li>
          <li><strong>204:</strong> No content. La petición se ha procesado correctamente, pero la respuesta no tiene contenido.</li>
          <li><strong>302:</strong> Redirect. La respuesta será redirigir a otra URL.</li>
          <li><strong>400:</strong> Bad Request. La petición del cliente es errónea, así que el servidor no la ha procesado.</li>
          <li><strong>404:</strong> Not found. El recurso solicitado no existe.</li>
        </ul>
      En la variable error hay almacenado un código de error. Escribe un script que muestre por pantalla el nombre de ese error, o un mensaje genérico si no lo reconoce. Cambia el valor de la variable para asegurarte de que funciona.
    </div>
    <div class="caja solucion">
        <h3>Solución</h3>
        <?php
          $error = 404;
        ?>
    </div>
  </body>
</html>

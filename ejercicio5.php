<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tema 2 - Ejercicio 5</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Ejercicio 5 - Variables vacías</h1>
    <div class="caja enunciado">
      Para la variable a, dale valores que hagan que esté vacía.
      <ul>
        <li>Una cadena de texto.</li>
        <li>Un valor entero.</li>
        <li>Un valor decimal.</li>
        <li>Un valor booleano.</li>
        <li>Un valor de array.</li>
        <li>Un valor nulo.</li>
      </ul>
      Cambia el valor de a para cada uno de los tipos de datos anteriores, y comprueba el mensaje que aparece en pantalla.
    </div>
    <div class="caja solucion">
        <h3>Solución</h3>
        <?php
        // Cambia el valor de a para que esté vacía
        $a = "valor";
        if(empty($a)){
          print "a está vacía";
        }
        else{
          print "a no está vacía";
        }
        ?>
    </div>
  </body>
</html>

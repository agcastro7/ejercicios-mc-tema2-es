<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tema 2 - Ejercicio 1</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Ejercicio 1 - Cadenas con formato</h1>
    <div class="caja enunciado">
      Los primeros 10 decimales del número de Euler son los siguientes: <br>
      2.7182818284 <br>
      Guarda el valor en una variable, y a continuación muéstralo mediante cadenas con los siguientes formatos:
        <ul>
          <li>Con sólo dos decimales: 2.71</li>
          <li>Con tres decimales y signo: +2.71</li>
          <li>Con 5 decimales y el resto relleno de ceros (un total de 10 caracteres): 2.718280000</li>
          <li>Sin decimales, un total de 10 caracteres y alineado a la derecha, con ceros a la izquierda: 0000000002</li>
        </ul>
    </div>
    <div class="caja solucion">
        <h3>Solución</h3>
        <?php
        
        ?>
    </div>
  </body>
</html>
